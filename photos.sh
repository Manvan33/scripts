#!/bin/bash

dir="$(pwd)"

function count {
    ls -Ub1 -- "$1" | wc -l
}

if [ "$1" = "delete" ]; then
    if [ ! -d "./jpegs" ]; then
        echo "Missing ./jpegs directory"
        exit 1
    fi
    mkdir -p "./done"
    echo "Filtering RW2 files..."
    echo "Found $(count ./jpegs) jpegs"
    ls ./jpegs | cut -d "." -f 1 | while read file; do
        mv "./$file".RW2 "./done/" 2> /dev/null
    done
    echo "Found $(count ./done) raws in ./done"
    echo "Remaining $(count ./) files in current directory"

else
    echo "Creating folders..."
    mkdir -p "./jpegs"
    mkdir -p "./vids"
    mv *.JPG ./jpegs/ 2> /dev/null
    mv *.MP4 ./vids/ 2> /dev/null
    echo "Found $(count ./jpegs) jpegs"
    echo "Found $(count ./vids) videos"
    echo "Remaining $(count ./) files in current directory"
fi
