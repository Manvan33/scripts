main="DisplayPort-2"
secondary="DisplayPort-0"
#secondary="HDMI-A-0"

xrandr --output $main --off
xrandr --output $secondary --off

xrandr --output $main --mode 2560x1440 --rate 144 --primary --left-of $secondary
xrandr --output $secondary --mode 1920x1080 --rate 25
xrandr --output $secondary --mode 1920x1080 --rate 144 --rotate left --pos -1080x-305
